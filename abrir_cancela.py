# Firmware destinado a abrir a cancela quando o RFID for aprovado

import RPi.GPIO as GPIO
import time
import sys
import I2C_LCD_driver
import time

def main():
      
      with open('/dev/tty', 'r') as tty:
         while True:
            RFID_input = tty.readline().strip()
            if RFID_input in card:
                lcdi2c.lcd_display_string("Acesso permitido", 1,0)
                lcdi2c.lcd_display_string("                ", 2,0)
                print ('Codigo lido : {}'.format(RFID_input))
                GPIO.output(14,GPIO.LOW) 
                time.sleep(1) 
                GPIO.output(14,GPIO.HIGH)
                time.sleep(4) 
                lcdi2c.lcd_display_string(" Passe o cartao  ", 1,0)
                lcdi2c.lcd_display_string("   No leitor", 2,0)
               
                
            else:
                
                lcdi2c.lcd_display_string("  Acesso negado   ", 1,0)
                lcdi2c.lcd_display_string("                ", 2,0)
                time.sleep(3) 
                lcdi2c.lcd_display_string(" Passe o cartao  ", 1,0)
                lcdi2c.lcd_display_string("   No leitor", 2,0)
                

lcdi2c = I2C_LCD_driver.lcd()

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(14,GPIO.OUT) 
lcdi2c.lcd_display_string(" Passe o cartao  ", 1,0)
lcdi2c.lcd_display_string("   No leitor ", 2,0)

main()
    
  
